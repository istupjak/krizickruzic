﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp2.Shapes;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        bool player1 = true;
        bool player2 = false;
        int[,] array = new int[,]
        {
            {0,0,0},
            {0,0,0},
            {0,0,0}

        };

        Pen pen = new Pen(Color.Black, 3.5F);
        public Form1()
        {
            InitializeComponent();
        }

        private void DrawPlayer(Graphics g, int n, int m)
        {
            if (player1)
            {
                Cross cr = new Cross();
                cr.draw(g, pen);
                array[n, m] = 1;
                if (Check(1))
                {
                    int scr;
                    scr = int.Parse(lblP1Score.Text);
                    scr += 1;
                    lblP1Score.Text = scr.ToString();
                    player1 = false;
                    player2 = true;

                    MessageBox.Show(tbP1Name.Text + " Wins this round!");
                    RstTable();
                    RstGame();
                    lblPTurn.Text = tbP2Name.Text + "'s turn";
                }
                else if (!Check(1) && !Check(2) && isDraw())
                {
                    player1 = false;
                    player2 = true;

                    MessageBox.Show("It's Draw");
                    RstTable();
                    RstGame();
                    lblPTurn.Text = tbP2Name.Text + "'s turn";
                }

                else
                {
                    player1 = false;
                    player2 = true;
                    lblPTurn.Text = tbP2Name.Text + "'s turn";
                }

            }

            else if (player2)
            {
                Circle c = new Circle();
                c.draw(g, pen);
                array[n, m] = 2;
                if (Check(2))
                {
                    int scr;
                    scr = int.Parse(lblP2Score.Text);
                    scr += 1;
                    lblP2Score.Text = scr.ToString();
                    player1 = true;
                    player2 = false;

                    MessageBox.Show(tbP2Name.Text + " Wins this round!");
                    RstTable();
                    RstGame();
                    lblPTurn.Text = tbP1Name.Text + "'s turn";
                }
                else if (!Check(1) && !Check(2) && isDraw())
                {
                    player1 = true;
                    player2 = false;

                    MessageBox.Show("It's Draw");
                    RstTable();
                    RstGame();
                    lblPTurn.Text = tbP1Name.Text + "'s turn";
                }

                else
                {
                    player1 = true;
                    player2 = false;
                    lblPTurn.Text = tbP1Name.Text + "'s turn";
                }
            }

        }

        private bool isDraw()
        {
            bool Draw = true;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (array[i, j] == 0)
                    {
                        Draw = false;
                        break;
                    }
                }

                if (!Draw)
                    break;
            }

            return Draw;
        }

        private void RstTable()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                    array[i, j] = 0;
            }
        }
        private void RstGame()
        {
            pictureBox1.ImageLocation = "";
            pictureBox2.ImageLocation = "";
            pictureBox3.ImageLocation = "";
            pictureBox4.ImageLocation = "";
            pictureBox5.ImageLocation = "";
            pictureBox6.ImageLocation = "";
            pictureBox7.ImageLocation = "";
            pictureBox8.ImageLocation = "";
            pictureBox9.ImageLocation = "";
        }

        private bool Check(int player)
        {


            if (RowsCheck(player))
                return true;
            else if (ColumnsCheck(player))
                return true;
            else if (MainDiagonalCheck(player))
                return true;
            else if (SideDiagonalCheck(player))
                return true;

            return false;
        }

        private bool RowsCheck(int player)
        {
            bool found = false;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (array[i, j] != player)
                    {
                        found = false;
                        break;
                    }
                    else
                        found = true;
                }

                if (found)
                    break;
            }

            return found;
        }

        private bool ColumnsCheck(int player)
        {
            bool found = false;
            for (int i = 0; i< 3; i++)
                {
                    for (int j = 0; j< 3; j++)
                    {
                        if (array[j, i] != player)
                        {
                            found = false;
                            break;
                        }
                        else
                            found = true;
                    }

                    if (found)
                        break;
            }
            return found;

        }


        private bool MainDiagonalCheck(int player)
        {
            bool found = false;
            int k = 0;
            for (int i = 0; i < 3; i++)
            {
                
                if (array[i, k] != player)
                {
                    found = false;
                    break;
                }
                else
                    found = true;
                k += 1;
            }
            return found;
        }

        private bool SideDiagonalCheck(int player)
        {
            bool found = false;
            int k = 2;
            for (int i = 0; i < 3; i++)
            {
                
                if (array[i, k] != player)
                {
                    found = false;
                    break;
                }
                else
                    found = true;
                k--;
            }

            return found;
        }

        private void btnNewGame_Click(object sender, EventArgs e)
        {
            if (tbP1Name.Text == "" || tbP2Name.Text == "")
            {
                MessageBox.Show("Please insert players names", "Where are players names?");
            }
            else
            {
                tbP1Name.Enabled = false;
                tbP2Name.Enabled = false;
                pictureBox1.ImageLocation = "";
                pictureBox2.ImageLocation = "";
                pictureBox3.ImageLocation = "";
                pictureBox4.ImageLocation = "";
                pictureBox5.ImageLocation = "";
                pictureBox6.ImageLocation = "";
                pictureBox7.ImageLocation = "";
                pictureBox8.ImageLocation = "";
                pictureBox9.ImageLocation = "";
                pictureBox1.Enabled = true;
                pictureBox2.Enabled = true;
                pictureBox3.Enabled = true;
                pictureBox4.Enabled = true;
                pictureBox5.Enabled = true;
                pictureBox6.Enabled = true;
                pictureBox7.Enabled = true;
                pictureBox8.Enabled = true;
                pictureBox9.Enabled = true;
                lblPTurn.Text = tbP1Name.Text + "'s Turn";
                btnReset.Enabled = true;
                btnNewGame.Enabled = false;
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            tbP1Name.Enabled = true;
            tbP2Name.Enabled = true;
            tbP1Name.Text = "";
            tbP2Name.Text = "";
            pictureBox1.ImageLocation = "";
            pictureBox2.ImageLocation = "";
            pictureBox3.ImageLocation = "";
            pictureBox4.ImageLocation = "";
            pictureBox5.ImageLocation = "";
            pictureBox6.ImageLocation = "";
            pictureBox7.ImageLocation = "";
            pictureBox8.ImageLocation = "";
            pictureBox9.ImageLocation = "";
            pictureBox1.Enabled = false;
            pictureBox2.Enabled = false;
            pictureBox3.Enabled = false;
            pictureBox4.Enabled = false;
            pictureBox5.Enabled = false;
            pictureBox6.Enabled = false;
            pictureBox7.Enabled = false;
            pictureBox8.Enabled = false;
            pictureBox9.Enabled = false;
            lblP1Score.Text = "0";
            lblP2Score.Text = "0";
            lblPTurn.Text = "";
            player1 = true;
            player2 = false;
            RstTable();
            btnReset.Enabled = false;
            btnNewGame.Enabled = true;

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pb1_MouseUp(object sender, MouseEventArgs e)
        {
            Graphics g;
            g = pictureBox1.CreateGraphics();
            DrawPlayer(g, 0, 0);
            
        }

        private void pb_4_MouseUp(object sender, MouseEventArgs e)
        {
            Graphics g;
            g = pictureBox4.CreateGraphics();
            DrawPlayer(g, 0, 1);
        }

        private void pb3_MouseUp(object sender,MouseEventArgs e)
        {
            Graphics g;
            g = pictureBox3.CreateGraphics();
            DrawPlayer(g, 2, 0);
        }

        private void pb2_MouseUp(object sender, MouseEventArgs e)
        {

            Graphics g;
            g = pictureBox2.CreateGraphics();
            DrawPlayer(g, 1, 0);
        }
        private void pb5_MouseUp(object sender, MouseEventArgs e)
        {
            Graphics g;
            g = pictureBox5.CreateGraphics();
            DrawPlayer(g, 1, 1);
        }

        private void pb6_MouseUp(object sender, MouseEventArgs e)
        {
            Graphics g;
            g = pictureBox6.CreateGraphics();
            DrawPlayer(g, 2, 1);
        }

        private void pb7_MouseUp(object sender, MouseEventArgs e)
        {
            Graphics g;
            g = pictureBox7.CreateGraphics();
            DrawPlayer(g, 0, 2);
        }

        private void pb8_MouseUp(object sender, MouseEventArgs e)
        {
            Graphics g;
            g = pictureBox8.CreateGraphics();
            DrawPlayer(g, 1, 2);
        }

        private void pb9_MouseUp(object sender, MouseEventArgs e)
        {
            Graphics g;
            g = pictureBox9.CreateGraphics();
            DrawPlayer(g, 2, 2);
        }
    }
}

