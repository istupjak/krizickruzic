﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace WindowsFormsApp2.Shapes
{
    class Circle
    {
        int r;
        public Circle()
        {
            r = 40;
            
        }
        public void draw(Graphics g, Pen p)
        {
            g.DrawEllipse(p, 10, 10, r, r);
        }
    }
}
