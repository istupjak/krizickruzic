﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace WindowsFormsApp2.Shapes
{
    class Cross
    {

        public void draw(Graphics g, Pen p)
        {
            g.DrawLine(p, 10, 10, 50, 50);
            g.DrawLine(p, 50, 10, 10, 50);
        }
    }
}
